import React, { useCallback } from 'react';

import { User as FirebaseUser } from 'firebase/auth';
import {
  Authenticator,
  buildCollection,
  buildProperty,
  EntityReference,
  FirebaseCMSApp,
} from 'firecms';

import '@fontsource/ibm-plex-mono';
import 'typeface-rubik';

import { CustomLoginView } from './CustomLoginView';

const {
  VITE_FIREBASE_API_KEY,
  VITE_FIREBASE_AUTH_DOMAIN,
  VITE_FIREBASE_PROJECT_ID,
  VITE_FIREBASE_STORAGE_BUCKET,
  VITE_FIREBASE_MESSAGING_SENDER_ID,
  VITE_FIREBASE_APP_ID,
  VITE_FIREBASE_MEASUREMENT_ID,
} = import.meta.env;

// TODO: Replace with your config
const firebaseConfig = {
  apiKey: VITE_FIREBASE_API_KEY || null,
  authDomain: VITE_FIREBASE_AUTH_DOMAIN || null,
  projectId: VITE_FIREBASE_PROJECT_ID || null,
  storageBucket: VITE_FIREBASE_STORAGE_BUCKET || null,
  messagingSenderId: VITE_FIREBASE_MESSAGING_SENDER_ID || null,
  appId: VITE_FIREBASE_APP_ID || null,
  measurementId: VITE_FIREBASE_MEASUREMENT_ID || null,
};

const locales = {
  'en-US': 'English (United States)',
};

type Solution = {
  date: string;
  szozat: string;
  szoreggelt: string;
  date_created?: number;
};

type Product = {
  name: string;
  price: number;
  status: string;
  published: boolean;
  related_products: EntityReference[];
  main_image: string;
  tags: string[];
  description: string;
  categories: string[];
  publisher: {
    name: string;
    external_id: string;
  };
  expires_on: Date;
};

const localeCollection = buildCollection({
  path: 'locale',
  customId: locales,
  name: 'Locales',
  singularName: 'Locales',
  properties: {
    name: {
      name: 'Title',
      validation: { required: true },
      dataType: 'string',
    },
    selectable: {
      name: 'Selectable',
      description: 'Is this locale selectable',
      dataType: 'boolean',
    },
    video: {
      name: 'Video',
      dataType: 'string',
      validation: { required: false },
      storage: {
        storagePath: 'videos',
        acceptedFiles: ['video/*'],
      },
    },
  },
});

// Creates default date for the form
const today = new Date();
const month = `0${today.getMonth() + 1}`.slice(-2);
const day = `0${today.getDate()}`.slice(-2);
const defaultDate = `${today.getFullYear()}-${month}-${day}`;

const solutionsCollection = buildCollection<Solution>({
  name: 'Megfejtések',
  singularName: 'Megfejtés',
  path: 'solutions',
  permissions: ({ authController }) => ({
    edit: true,
    create: true,
    // we have created the roles object in the navigation builder
    delete: false,
  }),
  initialSort: ['date', 'desc'],
  properties: {
    date: {
      name: 'Dátum',
      validation: { required: true },
      dataType: 'string',
      description: 'Formátum minta: "2023-02-27"',
      defaultValue: defaultDate,
    },
    szozat: {
      name: 'Szózat',
      validation: { required: true },
      dataType: 'string',
    },
    szoreggelt: {
      name: 'Szó Reggelt!',
      validation: { required: true },
      dataType: 'string',
    },
  },
});

const productsCollection = buildCollection<Product>({
  name: 'Products',
  singularName: 'Product',
  path: 'products',
  permissions: ({ authController }) => ({
    edit: true,
    create: true,
    // we have created the roles object in the navigation builder
    delete: false,
  }),
  subcollections: [localeCollection],
  properties: {
    name: {
      name: 'Name',
      validation: { required: true },
      dataType: 'string',
    },
    price: {
      name: 'Price',
      validation: {
        required: true,
        requiredMessage: 'You must set a price between 0 and 1000',
        min: 0,
        max: 1000,
      },
      description: 'Price with range validation',
      dataType: 'number',
    },
    status: {
      name: 'Status',
      validation: { required: true },
      dataType: 'string',
      description: 'Should this product be visible in the website',
      longDescription:
        'Example of a long description hidden under a tooltip. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin quis bibendum turpis. Sed scelerisque ligula nec nisi pellentesque, eget viverra lorem facilisis. Praesent a lectus ac ipsum tincidunt posuere vitae non risus. In eu feugiat massa. Sed eu est non velit facilisis facilisis vitae eget ante. Nunc ut malesuada erat. Nullam sagittis bibendum porta. Maecenas vitae interdum sapien, ut aliquet risus. Donec aliquet, turpis finibus aliquet bibendum, tellus dui porttitor quam, quis pellentesque tellus libero non urna. Vestibulum maximus pharetra congue. Suspendisse aliquam congue quam, sed bibendum turpis. Aliquam eu enim ligula. Nam vel magna ut urna cursus sagittis. Suspendisse a nisi ac justo ornare tempor vel eu eros.',
      enumValues: {
        private: 'Private',
        public: 'Public',
      },
    },
    published: ({ values }) =>
      buildProperty({
        name: 'Published',
        dataType: 'boolean',
        columnWidth: 100,
        disabled:
          values.status === 'public'
            ? false
            : {
                clearOnDisabled: true,
                disabledMessage:
                  'Status must be public in order to enable this the published flag',
              },
      }),
    related_products: {
      dataType: 'array',
      name: 'Related products',
      description: 'Reference to self',
      of: {
        dataType: 'reference',
        path: 'products',
      },
    },
    main_image: buildProperty({
      // The `buildProperty` method is a utility function used for type checking
      name: 'Image',
      dataType: 'string',
      storage: {
        storagePath: 'images',
        acceptedFiles: ['image/*'],
      },
    }),
    tags: {
      name: 'Tags',
      description: 'Example of generic array',
      validation: { required: true },
      dataType: 'array',
      of: {
        dataType: 'string',
      },
    },
    description: {
      name: 'Description',
      description: "Not mandatory but it'd be awesome if you filled this up",
      longDescription:
        'Example of a long description hidden under a tooltip. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin quis bibendum turpis. Sed scelerisque ligula nec nisi pellentesque, eget viverra lorem facilisis. Praesent a lectus ac ipsum tincidunt posuere vitae non risus. In eu feugiat massa. Sed eu est non velit facilisis facilisis vitae eget ante. Nunc ut malesuada erat. Nullam sagittis bibendum porta. Maecenas vitae interdum sapien, ut aliquet risus. Donec aliquet, turpis finibus aliquet bibendum, tellus dui porttitor quam, quis pellentesque tellus libero non urna. Vestibulum maximus pharetra congue. Suspendisse aliquam congue quam, sed bibendum turpis. Aliquam eu enim ligula. Nam vel magna ut urna cursus sagittis. Suspendisse a nisi ac justo ornare tempor vel eu eros.',
      dataType: 'string',
      columnWidth: 300,
    },
    categories: {
      name: 'Categories',
      validation: { required: true },
      dataType: 'array',
      of: {
        dataType: 'string',
        enumValues: {
          electronics: 'Electronics',
          books: 'Books',
          furniture: 'Furniture',
          clothing: 'Clothing',
          food: 'Food',
        },
      },
    },
    publisher: {
      name: 'Publisher',
      description: 'This is an example of a map property',
      dataType: 'map',
      properties: {
        name: {
          name: 'Name',
          dataType: 'string',
        },
        external_id: {
          name: 'External id',
          dataType: 'string',
        },
      },
    },
    expires_on: {
      name: 'Expires on',
      dataType: 'date',
    },
  },
});

export default function App() {
  const myAuthenticator: Authenticator<FirebaseUser> = useCallback(
    async ({ user, authController }) => {
      if (user?.email?.includes('flanders')) {
        throw Error('Stupid Flanders!');
      }

      console.log('Allowing access to', user?.email);
      // This is an example of retrieving async data related to the user
      // and storing it in the user extra field.
      const sampleUserRoles = await Promise.resolve(['admin']);
      authController.setExtra(sampleUserRoles);

      return true;
    },
    []
  );

  return (
    <FirebaseCMSApp
      name={'Szavazz! CMS'}
      authentication={myAuthenticator}
      collections={[solutionsCollection]}
      firebaseConfig={firebaseConfig}
      signInOptions={['password']}
      LoginView={CustomLoginView}
    />
  );
}
