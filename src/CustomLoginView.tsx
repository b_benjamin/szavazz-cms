import { FirebaseLoginView, FirebaseLoginViewProps } from 'firecms';

export function CustomLoginView(props: FirebaseLoginViewProps) {
  return (
    <FirebaseLoginView
      {...props}
      disableSignupScreen={true}
      noUserComponent={<>User does not exists.</>}
    />
  );
}
